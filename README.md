# Cats Effect Syntax

Additional syntax for [Cats Effect](https://typelevel.org/cats-effect/)

## Quick Start

To use within an [sbt](https://www.scala-sbt.org/) project, add the following
to your build.sbt file:

```sbt
resolvers +=
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"

libraryDependencies +=
  "com.tylip" %% "cats-effect-syntax" % version
```

Substituting the `version` with a preferred version of the library.
See [Releases](../../releases) for available versions.

## Usage

```scala
import com.tylip.catseffect.syntax._
```

Having an `x: F[A]` where `F: Applicative` use

```scala
x.resource
```

instead of

```scala
Resource.liftF(x)
```
