val scala213 = "2.13.5"
val scala212 = "2.12.15"

val tylipPublic =
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"
ThisBuild / publishTo := Some(tylipPublic)
credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

lazy val core = (projectMatrix in file("core"))
  .jvmPlatform(scalaVersions = Seq(scala213, scala212))
  .settings(
    name := "cats-effect-syntax",
    organization := "com.tylip",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect" % "2.3.3",
    ),
    resolvers += tylipPublic,
    mimaPreviousArtifacts :=
        previousStableVersion.value.map(organization.value %% moduleName.value % _).toSet,
    scalafmtOnCompile := !insideCI.value,
    scalacOptions ++= Seq(
      "-deprecation", // Emit warning and location for usages of deprecated APIs.
      "-feature", // Emit warning and location for usages of features that should be imported explicitly.
      "-unchecked", // Enable additional warnings where generated code depends on assumptions.
      "-Ywarn-dead-code", // Warn when dead code is identified.
    ),
  )

lazy val core212 = core.finder()(scala212)
  .settings(
    scalacOptions ++= Seq("-language:higherKinds"),
  )

lazy val root = (project in file("."))
  .aggregate(core.projectRefs : _*)
  .settings(
    mimaPreviousArtifacts := Set.empty,
  )
