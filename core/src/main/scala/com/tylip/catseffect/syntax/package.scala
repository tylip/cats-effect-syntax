package com.tylip.catseffect

import cats.Applicative
import cats.effect.Resource

package object syntax {

  implicit class ApplicativeEffectOps[F[_], A](val fa: F[A]) extends AnyVal {
    def resource(implicit F: Applicative[F]): Resource[F, A] =
      Resource.liftF(fa)
  }

}
